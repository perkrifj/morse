package dev;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

public class SoundThread extends Thread implements Runnable{

	private byte[] sound;
	private int sampleRate;
	private SourceDataLine source;

	public SoundThread(byte[] sounds, int sampleRate){
		this.sound = sounds;
		this.sampleRate = sampleRate;
	}
	
	public void stopPlayout(){
		this.source.stop();
	}

	public void run() {

		AudioFormat af = new AudioFormat( (float)sampleRate, 8, 1, true, true );
		DataLine.Info info = new DataLine.Info ( SourceDataLine.class, af );
		source = null;
		try {
			source = (SourceDataLine) AudioSystem.getLine( info );
			source.open( af );
			source.start();
			source.write( sound, 0, sound.length );
			source.drain();
			source.close();
		} catch (LineUnavailableException e1NullPointerException) {
			System.err.println("Unable to produce sound because there is no DataLine to sound!.");
//			e1.printStackTrace();
		}
		catch(NullPointerException npe){
			System.err.println("Unable to produce sound because the source is null!");
		}
		

	}
}
