package dev;


public class Sound{

	private int sampleRate = 8000;
	private double frequency = 1000;
	private double rad = 2.0 * Math.PI;
	private int amplitude = 50;
	/**
	 * Length of a '.' in milliseconds
	 */
	private int dotLength;
	private byte[] sound = new byte[0];
	
	
	/**
	 * 
	 * @param s Takes a string as parapeter. This string must be valid morse!
	 */
	public Sound(String s, int speed){
		this.dotLength = speed;
		buildSound(s);
		playSound(sound);
	}

	public byte[] toneGenerator(int length){
		byte[] buf = new byte[((sampleRate * (length*dotLength))/1000)];
		//		System.out.println(buf.length);
		for (int i=0; i<buf.length; i++) {
			buf[i] = (byte)( Math.sin( rad * frequency / sampleRate * i ) * amplitude );
		}
		return buf;
	}

	public byte[] stopSpaceGenerator(int length){
		byte[] buf = new byte[((sampleRate * (length*dotLength))/1000)];
		for (int i = 0; i < buf.length; i++) {
			buf[i] = 0;
		}
		return buf;
	}


	public void buildSound(String morse){
		System.out.println(morse);
		//		Remove trailing whitespace
		morse = morse.trim();
		System.out.println(morse);
		for (int i = 0; i < morse.length(); i++) {
			if(morse.charAt(i) == '.'){
				appendToSoundArray(toneGenerator(1));
				if(i != morse.length()-1)
					appendToSoundArray(stopSpaceGenerator(1));
			}
			else if(morse.charAt(i) == '-'){
				appendToSoundArray(toneGenerator(3));
				if(i != morse.length()-1)
					appendToSoundArray(stopSpaceGenerator(1));
			}
			else if(morse.charAt(i) == ' '){
				appendToSoundArray(stopSpaceGenerator(3));
			}
			/*
			 * GAPS BETWEEN WORDS - I.E WHEN AN '.' WAS ENTERED IN TEXT MODE!
			 */
			else if(morse.charAt(i) == '@'){
				appendToSoundArray(stopSpaceGenerator(7));
			}
		}
	}

	public void appendToSoundArray(byte[] arrayToAppend){
		byte[] temp = new byte[sound.length + arrayToAppend.length];
		for (int i = 0; i < sound.length; i++) {
			temp[i] = sound[i];
		}
		for (int i = 0; i < arrayToAppend.length; i++) {
			temp[i+sound.length] = arrayToAppend[i];
		}
		sound = temp;
	}
	
	public void playSound(byte[] sound){
		Thread t = new Thread(new SoundThread(sound, sampleRate));
		t.start();
	}

	public static void main ( String[] args ) throws InterruptedException{

		Sound st = new Sound("... --- ... ", 40);
//		st.buildSound("... --- ... ");
		Thread t = new Thread(new SoundThread(st.sound, st.sampleRate));
		t.start();
		System.out.println("MULTIPLE THREADS");
		//		for (int i = 0; i < st.sound.length; i++) {
		//			System.out.println(st.sound[i]);
		//		}
//		st.playSound(st.sound);
	}
}

