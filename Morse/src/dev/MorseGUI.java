package dev;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

public class MorseGUI extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7793234101362791005L;

	JPanel textPanel;
	JPanel buttonPanel;
	JPanel soundButtonPanel;
	JTextField textArea;
	JTextField disabledTextArea;
	JRadioButton textRadioButton;
	JRadioButton morseRadioButton;
	JSpinner morseSpeed;

	public MorseGUI(){

		/*
		 * TEXT COMPONENTS
		 */
		textPanel = new JPanel();
		textArea = new JTextField(20);
		disabledTextArea = new JTextField(20);

		disabledTextArea.setEditable(false);
		disabledTextArea.setBackground(Color.GRAY);
		disabledTextArea.setFont(new Font("Verdana", Font.PLAIN, 16));
		textArea.setFont(new Font("Verdana", Font.PLAIN, 16));

		textPanel.setLayout(new BorderLayout());
		textPanel.add(textArea, BorderLayout.NORTH);
		textPanel.add(disabledTextArea, BorderLayout.SOUTH);

		buttonPanel = new JPanel();

		/*
		 * TRANSLATE BUTTONS
		 */
		JButton translateButton = new JButton("Translate");
		translateButton.addActionListener(this);
		translateButton.setActionCommand("TRANSLATE");
		buttonPanel.add(translateButton);

		morseRadioButton = new JRadioButton("Morse");
		textRadioButton = new JRadioButton("Text", true);

		ButtonGroup bg = new ButtonGroup();
		bg.add(morseRadioButton);
		bg.add(textRadioButton);

		buttonPanel.add(morseRadioButton);
		buttonPanel.add(textRadioButton);


		/*
		 * SOUND BUTTONS
		 */
		soundButtonPanel = new JPanel();
		JButton playSoundButton = new JButton("Play");
		playSoundButton.addActionListener(this);
		playSoundButton.setActionCommand("PLAY");
		soundButtonPanel.add(playSoundButton);

		morseSpeed = new JSpinner(new SpinnerNumberModel(50,25, 100, 1));
		soundButtonPanel.add(morseSpeed);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println(e.getActionCommand());
		if (e.getActionCommand().equals("TRANSLATE")) {
			String text = textArea.getText();
			if(text != null){
				if(morseRadioButton.isSelected())
					disabledTextArea.setText(Morse.stringFromMorse(text));
				if(textRadioButton.isSelected())
					disabledTextArea.setText(Morse.morseFromString(text));
			}
		}
		else if(e.getActionCommand().equals("PLAY")){
			System.out.println("PLAY PRESSED");
			String text = textArea.getText();
			if(text != null){
				if(morseRadioButton.isSelected())
					new Sound(textArea.getText(), (Integer) morseSpeed.getValue());
				if(textRadioButton.isSelected())
					new Sound(Morse.morseFromString(textArea.getText()), (Integer) morseSpeed.getValue());
			}
		}
	}

	public static void main(String[] args) {
		MorseGUI mg = new MorseGUI();
		JFrame frame = new JFrame("Morse APP - Per Kristian Fjellby");
		frame.setLayout(new BorderLayout());
		frame.add(mg.textPanel, BorderLayout.NORTH);
		frame.add(mg.buttonPanel, BorderLayout.CENTER);
		frame.add(mg.soundButtonPanel, BorderLayout.SOUTH);
		frame.setVisible(true);
		frame.pack();
	}
}
