package dev;

/**
 * @author Per Kristian Fjellby
 */
public class Morse {

	public final static String Alpha = ".-";
	public final static String Beta = "-...";
	public final static String Charlie = "-.-.";
	public final static String Delta = "-..";
	public final static String Echo = ".";
	public final static String Foxtrot = "..-.";
	public final static String Golf = "--.";
	public final static String Hotel = "....";
	public final static String India = "..";
	public final static String Juliet = ".---";
	public final static String Kilo = "-.-";
	public final static String Lima = ".-..";
	public final static String Mike = "--";
	public final static String November = "-.";
	public final static String Oscar = "---";
	public final static String Papa = ".--.";
	public final static String Quebec = "--.-";
	public final static String Romeo = ".-.";
	public final static String Sierra = "...";
	public final static String Tango = "-";
	public final static String Union = "..-";
	public final static String Victor = "...-";
	public final static String Whiskey = ".--";
	public final static String Extra = "-..-";
	public final static String Yankee = "-.--";
	public final static String Zulu = "--..";
	public final static String ONE = ".----";
	public final static String TWO = "..---";
	public final static String THREE = "...--";
	public final static String FOUR = "....-";
	public final static String FIVE = ".....";
	public final static String SIX = "-....";
	public final static String SEVEN = "--...";
	public final static String EIGHT = "---..";
	public final static String NINE = "----.";
	public final static String ZERO = "-----";
	
	public final static char DOT = '.';


	public static String returnMorse(char c){
		String s = Character.toString(c);
		if(s.equals("A")){
			return Alpha;
		}
		else if(s.equals("B")){
			return Beta;
		}
		else if(s.equals("C")){
			return Charlie;
		}
		else if(s.equals("D")){
			return Delta;
		}
		else if(s.equals("E")){
			return Echo;
		}
		else if(s.equals("F")){
			return Foxtrot;
		}
		else if(s.equals("G")){
			return Golf;
		}
		else if(s.equals("H")){
			return Hotel;
		}
		else if(s.equals("I")){
			return India;
		}
		else if(s.equals("J")){
			return Juliet;
		}
		else if(s.equals("K")){
			return Kilo;
		}
		else if(s.equals("L")){
			return Lima;
		}
		else if(s.equals("M")){
			return Mike;
		}
		else if(s.equals("N")){
			return November;
		}
		else if(s.equals("O")){
			return Oscar;
		}
		else if(s.equals("P")){
			return Papa;
		}
		else if(s.equals("Q")){
			return Quebec;
		}
		else if(s.equals("R")){
			return Romeo;
		}
		else if(s.equals("S")){
			return Sierra;
		}
		else if(s.equals("T")){
			return Tango;
		}
		else if(s.equals("U")){
			return Union;
		}
		else if(s.equals("V")){
			return Victor;
		}
		else if(s.equals("W")){
			return Whiskey;
		}
		else if(s.equals("X")){
			return Extra;
		}
		else if(s.equals("Y")){
			return Yankee;
		}
		else if(s.equals("Z")){
			return Zulu;
		}
		else if(s.equals("0")){
			return ZERO;
		}
		else if(s.equals("1")){
			return ONE;
		}
		else if(s.equals("2")){
			return TWO;
		}
		else if(s.equals("3")){
			return THREE;
		}
		else if(s.equals("4")){
			return FOUR;
		}
		else if(s.equals("5")){
			return FIVE;
		}
		else if(s.equals("6")){
			return SIX;
		}
		else if(s.equals("7")){
			return SEVEN;
		}
		else if(s.equals("8")){
			return EIGHT;
		}
		else if(s.equals("9")){
			return NINE;
		}
		return "?";
	}
	
	private static String toCharacter(String s) {
		if(s.equals(Alpha)){
			return "A";
		}
		else if(s.equals(Beta)){
			return "B";
		}
		else if(s.equals(Charlie)){
			return "C";
		}
		else if(s.equals(Delta)){
			return "D";
		}
		else if(s.equals(Echo)){
			return "E";
		}
		else if(s.equals(Foxtrot)){
			return "F";
		}
		else if(s.equals(Golf)){
			return "G";
		}
		else if(s.equals(Hotel)){
			return "H";
		}
		else if(s.equals(India)){
			return "I";
		}
		else if(s.equals(Juliet)){
			return "J";
		}
		else if(s.equals(Kilo)){
			return "K";
		}
		else if(s.equals(Lima)){
			return "L";
		}
		else if(s.equals(Mike)){
			return "M";
		}
		else if(s.equals(November)){
			return "N";
		}
		else if(s.equals(Oscar)){
			return "O";
		}
		else if(s.equals(Papa)){
			return "P";
		}
		else if(s.equals(Quebec)){
			return "P";
		}
		else if(s.equals(Romeo)){
			return "R";
		}
		else if(s.equals(Sierra)){
			return "S";
		}
		else if(s.equals(Tango)){
			return "T";
		}
		else if(s.equals(Union)){
			return "U";
		}
		else if(s.equals(Victor)){
			return "V";
		}
		else if(s.equals(Whiskey)){
			return "W";
		}
		else if(s.equals(Extra)){
			return "X";
		}
		else if(s.equals(Yankee)){
			return "Y";
		}
		else if(s.equals(Zulu)){
			return "Z";
		}
		else if(s.equals(ONE)){
			return "1";
		}
		else if(s.equals(TWO)){
			return "2";
		}
		else if(s.equals(THREE)){
			return "3";
		}
		else if(s.equals(FOUR)){
			return "4";
		}
		else if(s.equals(FIVE)){
			return "5";
		}
		else if(s.equals(SIX)){
			return "6";
		}
		else if(s.equals(SEVEN)){
			return "7";
		}
		else if(s.equals(EIGHT)){
			return "8";
		}
		else if(s.equals(NINE)){
			return "9";
		}
		return "?";
	}

	public static String morseFromString(String s){
		s = s.toUpperCase();
		System.out.println(s);
		//Remove all internal whitespace!
		s = s.replaceAll("\\s", "");
		System.out.println(s);
		char[] array = s.toCharArray();
		String temp = "";
		for (int i = 0; i < array.length; i++) {
			temp += returnMorse(array[i]) + " ";
		}
		return temp;
	}
	/**
	 * Parses the input string to normal characters. The morse-string has to be valid in the sence that is contains a sequence of long and short beeps followed by a whitespace.
	 * Example: "ABCD" will be ".- -... -.-. -.."
	 * 
	 * @param morse
	 * @return ASCII caracters
	 */
	public static String stringFromMorse(String morse){
		String tempString = "";
		String tempMorse = "";
		for (int i = 0; i < morse.length(); i++) {
			if(Character.toString(morse.charAt(i)).equals(" ")){
				tempString += toCharacter(tempMorse);
				tempMorse = "";
			}
			
			else if(i == morse.length()-1){
				tempMorse += morse.charAt(i);
				tempString += toCharacter(tempMorse);
				tempMorse = "";
			}
			
			else tempMorse += morse.charAt(i);
		}
		return tempString;
	}


	public static void main(String[] args) {
		String testString = "... --- ...";

		System.out.println(morseFromString("LA6XTA"));
		System.out.println("-----------------------------------");
		System.out.println(stringFromMorse(testString));
	}
}


